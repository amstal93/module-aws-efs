#####
# EFS
#####

output "arn" {
  description = "Amazon Resource Name of the file system."
  value       = aws_efs_file_system.this.arn
}

output "id" {
  description = "The ID that identifies the file system (e.g. fs-ccfc0d65)."
  value       = aws_efs_file_system.this.id
}

output "dns_name" {
  description = "The DNS name for the filesystem."
  value       = aws_efs_file_system.this.dns_name
}

output "mount_target_network_interface_ids" {
  description = "The IDs of the network interfaces that Amazon EFS created when it created the mount targets."
  value       = aws_efs_mount_target.this.*.network_interface_id
}

output "mount_target_ids" {
  description = "The IDs of the EFS mount targets."
  value       = aws_efs_mount_target.this.*.id
}

output "mount_target_ips" {
  description = "List of mount target IPs"
  value       = data.aws_efs_mount_target.this.*.ip_address
}

#####
# KMS
#####

output "kms_key_id" {
  description = "The globally unique identifier for the EFS key. This output will be empty if the KMS key was passed as variable."
  value       = element(concat(aws_kms_key.this.*.id, [""]), 0)
}

output "kms_key_arn" {
  description = "The Amazon Resource Name (ARN) of the EFS key. This output will be empty if the KMS key was passed as variable."
  value       = element(concat(aws_kms_key.this.*.arn, [""]), 0)
}

output "kms_alias_arn" {
  description = "The Amazon Resource Name (ARN) of the EFS key alias. This output will be empty if the KMS key was passed as variable."
  value       = element(concat(aws_kms_alias.this.*.arn, [""]), 0)
}

#####
# SSM Parameter
#####

output "ssm_parameter_arns" {
  description = "The ARNs of the SSM Parameters for the EFS."
  value = compact(
    [
      element(concat(aws_ssm_parameter.this_efs_id.*.arn, [""]), 0),
    ],
  )
}

output "ssm_parameter_names" {
  description = "The names of the SSM Parameters for the EFS."
  value = compact(
    [
      element(concat(aws_ssm_parameter.this_efs_id.*.name, [""]), 0),
    ],
  )
}

#####
# Security group
#####

output "security_group_id" {
  description = "ID of the security group used for the EFS. This output will be empty if the security groups IDs were passed as variables."
  value       = element(concat(aws_security_group.this.*.id, [""]), 0)
}

output "security_group_arn" {
  description = "ARN of the security group used for the EFS. This output will be empty if the security groups IDs were passed as variables."
  value       = element(concat(aws_security_group.this.*.arn, [""]), 0)
}

output "security_group_rule_ids" {
  description = "List of ID's of the security rules added to security group"
  value       = concat(aws_security_group_rule.this_cidrs.*.id, aws_security_group_rule.this_security_groups.*.id)
}
