#####
# EFS
#####

output "arn" {
  value = module.one_zone_storage_policy.arn
}

output "id" {
  value = module.one_zone_storage_policy.id
}

output "dns_name" {
  value = module.one_zone_storage_policy.dns_name
}

output "mount_target_network_interface_ids" {
  value = module.one_zone_storage_policy.mount_target_network_interface_ids
}

output "mount_target_ids" {
  value = module.one_zone_storage_policy.mount_target_ids
}

output "mount_target_ips" {
  value = module.one_zone_storage_policy.mount_target_ips
}

#####
# KMS
#####

output "kms_key_id" {
  value = module.one_zone_storage_policy.kms_key_id
}

output "kms_key_arn" {
  value = module.one_zone_storage_policy.kms_key_arn
}

output "kms_alias_arn" {
  value = module.one_zone_storage_policy.kms_alias_arn
}

#####
# SSM Parameter
#####

output "ssm_parameter_arns" {
  value = module.one_zone_storage_policy.ssm_parameter_arns
}

output "ssm_parameter_names" {
  value = module.one_zone_storage_policy.ssm_parameter_names
}

#####
# Security group
#####

output "security_group_id" {
  value = module.one_zone_storage_policy.security_group_id
}

output "security_group_arn" {
  value = module.one_zone_storage_policy.security_group_arn
}
