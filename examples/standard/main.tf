resource "random_string" "this" {
  length  = 8
  upper   = false
  special = false
}

data "aws_vpc" "default" {
  default = true
}

data "aws_subnet_ids" "default" {
  vpc_id = data.aws_vpc.default.id

  filter {
    name   = "defaultForAz"
    values = ["true"]
  }

}

module "standard" {
  source = "../../"

  tags = {
    Name = "tftest${random_string.this.result}"
  }

  subnet_ids = data.aws_subnet_ids.default.ids

  name                = "tftest${random_string.this.result}"
  security_group_name = "tftest${random_string.this.result}"

  kms_key_alias_name = "alias/tftest/${random_string.this.result}"
  kms_key_name       = "tftest${random_string.this.result}"

  allowed_cidrs = ["10.0.0.0/8"]

  ssm_parameter_enabled = true
  ssm_parameter_prefix  = "/param/tftest${random_string.this.result}"
}
