resource "random_string" "this" {
  length  = 8
  upper   = false
  special = false
}

data "aws_vpc" "default" {
  default = true
}

data "aws_subnet_ids" "default" {
  vpc_id = data.aws_vpc.default.id

  filter {
    name   = "defaultForAz"
    values = ["true"]
  }
}

resource "aws_security_group" "with_external_kms_and_sgs" {
  name   = "tftest-${random_string.this.result}"
  vpc_id = data.aws_vpc.default.id
}

resource "aws_kms_key" "with_external_kms_and_sgs" {}

module "with_external_kms_and_sgs" {
  source = "../../"

  tags = {
    Name = "tftest${random_string.this.result}"
  }

  subnet_ids = [tolist(data.aws_subnet_ids.default.ids)[0]]

  name = "tftest${random_string.this.result}"

  kms_key_arn = aws_kms_key.with_external_kms_and_sgs.arn

  security_group_ids = [aws_security_group.with_external_kms_and_sgs.id]
}
