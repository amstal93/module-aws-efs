#####
# EFS
#####

output "arn" {
  value = module.with_external_kms_and_sgs.arn
}

output "id" {
  value = module.with_external_kms_and_sgs.id
}

output "dns_name" {
  value = module.with_external_kms_and_sgs.dns_name
}

output "mount_target_network_interface_ids" {
  value = module.with_external_kms_and_sgs.mount_target_network_interface_ids
}

output "mount_target_ids" {
  value = module.with_external_kms_and_sgs.mount_target_ids
}

output "mount_target_ips" {
  value = module.with_external_kms_and_sgs.mount_target_ips
}

#####
# KMS
#####

output "kms_key_id" {
  value = module.with_external_kms_and_sgs.kms_key_id
}

output "kms_key_arn" {
  value = module.with_external_kms_and_sgs.kms_key_arn
}

output "kms_alias_arn" {
  value = module.with_external_kms_and_sgs.kms_alias_arn
}

#####
# SSM Parameter
#####

output "ssm_parameter_arns" {
  value = module.with_external_kms_and_sgs.ssm_parameter_arns
}

output "ssm_parameter_names" {
  value = module.with_external_kms_and_sgs.ssm_parameter_names
}

#####
# Security group
#####

output "security_group_id" {
  value = module.with_external_kms_and_sgs.security_group_id
}

output "security_group_arn" {
  value = module.with_external_kms_and_sgs.security_group_arn
}
